import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Board {

    private int boardIncrement = 0, boardDemension = 10, boardRow = 0;
    private Object battleBoard[][] = new Object[boardDemension * boardDemension][2];
    private Scanner sc = new Scanner(System.in);

    private Boolean keepShooting = true;

    public Board() {
        // Make the board!
        for (int x = 0; x < battleBoard.length; x++) {
            boardRow++;
            if (boardRow > boardDemension) {
                boardRow = 1;
            }
            Object subArray[] = battleBoard[x];
            for (int y = 0; y < subArray.length; y++) {

                switch (y) {
                case 0:
                    if (x != 0 && x % boardDemension == 0) {
                        // System.out.println(x);
                        boardIncrement++;
                    }
                    subArray[y] = Character.toString((char) (65 + boardIncrement)) + Integer.toString(boardRow);
                    break;

                case 1:
                    // subArray[y] = "Battleship";
                    subArray[y] = "[   ]";
                    break;

                default:
                    break;
                }

            }
        }
    }

    public void placeShips(Ship[] ships) {
        // Place the ships on the board
        for (Ship ship : ships) {
            List<String> list = Arrays.asList(ship.getLocation());
            for (int x = 0; x < battleBoard.length; x++) {
                Object subArray[] = battleBoard[x];
                // System.out.println("Length of array " + x + " is " + subArray.length);
                if (list.contains(subArray[0])) {
                    subArray[1] = ship;
                }
            }
        }
    }

    public void fireShot(String calledLocation) {
        for (int x = 0; x < battleBoard.length; x++) {
            Object subArray[] = battleBoard[x];
            // System.out.println("Length of array " + x + " is " + subArray.length);
            if (subArray[0].toString().equals(calledLocation)) {
                if (subArray[1].getClass().getSimpleName().equals("Ship")) {
                    System.out.println("HIT!");
                    System.out.println();
                    ((Ship) subArray[1]).takeHit();
                    subArray[1] = " [X] ";

                } else {
                    System.out.println("Miss :(");
                    System.out.println();
                    subArray[1] = " [O] ";
                }
                break;
            }
        }

        this.print(false);
    }

    public void print(boolean showShips) {
        // Print the board
        System.out.println();
        for (int x = 0; x < battleBoard.length; x++) {
            Object subArray[] = battleBoard[x];
            // System.out.println("Length of array " + x + " is " + subArray.length);
            if (x == 0) {
                System.out.print(subArray[0].toString().charAt(0) + "\t");
            } else if (x % boardDemension == 0) {
                System.out.println();
                System.out.print(subArray[0].toString().charAt(0) + "\t");
            }
            if (showShips || subArray[1].getClass().getSimpleName().equals("String")) {
                System.out.print(subArray[1] + "\t");
            } else {
                System.out.print("[   ]" + "\t");
            }
        }

        // Print the numbers on the bottom of the board
        System.out.print("\r\t");
        for (int x = 1; x <= boardDemension; x++) {
            if (x < 10) {
                System.out.print("  " + x + "\t");
            } else {
                System.out.print("  " + x + "\t");
            }
        }
    }
}