import java.io.IOException;
import java.io.*;
import java.util.Scanner;

public class Battleship {

    private static Scanner sc = new Scanner(System.in);
    private static String playerName;
    private static boolean gameOver = false;

    public static void main(String[] args) {

        try {

            String record;

            // instantiating br here to point to the input file
            BufferedReader br = new BufferedReader(new FileReader("BaltteshipLogo.txt"));

            // reading the first record and storing it in the 'record' variable
            record = br.readLine();

            while (record != null) {
                System.out.println(record);
                record = br.readLine();
            }

            br.close();
            System.out.println();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

        System.out.println("Welcome to Battleship! A strategy type guessing game for two players. "
                + "It is played on ruled grids (paper or board) on which each player's fleet of ships (including battleships) "
                + "are marked. The locations of the fleets are concealed from the other player. Players alternate turns calling "
                + "'shots' at the other player's ships, and the objective of the game is to destroy the opposing player's fleet.");
        System.out.println();

        System.out.println("Are you ready to attack? (y/n)");
        String letsPlay = sc.nextLine();

        if (letsPlay.equalsIgnoreCase("y")) {
            System.out.println("Great, let's play.");
        } else {
            System.out.println("Too bad, lets go...");
        }
        System.out.println();

        // Set player one
        System.out.println("Player one, what is your name?");
        playerName = sc.nextLine();
        Player playerOne = new Player(playerName);
        clear();

        // Set player two
        System.out.println("Player two, what is your name?");
        playerName = sc.nextLine();
        Player playerTwo = new Player(playerName);
        System.out.println();
        clear();

        Player[] players = { playerOne, playerTwo };

        while (!gameOver) {
            if (players[0].allShipsSunk()) {
                gameOver = true;
                System.out.println(players[0].getPlayerName() + " has lost.");
            } else {
                players[1].printBoard();
                players[1].takeShot(players[0].callShot());
            }

            if (!gameOver) {
                if (players[1].allShipsSunk()) {
                    gameOver = true;
                    System.out.println(players[1].getPlayerName() + " has lost.");
                }
                players[0].printBoard();
                players[0].takeShot(players[1].callShot());
            }
        }
    }

    public static void clear() {
        for (int i = 0; i <= 20; i++)
        {
            System.out.println();
        }
    }

}