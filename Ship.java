public class Ship {

    private int hitCount = 0;
    private int maxHits;
    private String shipName;
    private String[] location;

    public Ship(String shipName, String location, int maxHits) {
        this.shipName = shipName;
        this.location = location.split(",");
        this.maxHits = maxHits;
    }

    public String[] getLocation() {
        return location;
    }

    public boolean isSunk() {
        return hitCount >= maxHits;
    }

    public void takeHit(){
        hitCount++;
        if (maxHits == hitCount)
        {
            System.out.println("You have sunk my " + shipName + " ship!!!");
        }
    }

    public String toString() {
        return shipName;
    }
}