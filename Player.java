import java.util.Random;
import java.util.Scanner;

public class Player {
    private Board playerBoard;
    private Ship[] playerShips;
    private String playerName;
    private String shotFired;

    public static Scanner sc = new Scanner(System.in);

    public Player(String playerName)
    {
        if (playerName != null && playerName.length() > 0)
        {
            this.playerName = playerName;
        } else
        {
            this.playerName = getRandomPlayerName();
        }

        System.out.println("Welcome " + this.playerName + ", good luck!");

        this.playerBoard = new Board();
        setPlayerShipLocations();
    }

    public String getRandomPlayerName(){
        Random rnd = new Random();
        int n = 10000 + rnd.nextInt(90000);
        return "Player" + Integer.toString(n);
    }

    public String getPlayerName(){
        return playerName;
    }

    public void setPlayerShipLocations(){
        // Make all the ships

        // Set the Carier
        System.out.println(playerName + ", please give Carier location?");
        Ship carier = new Ship("Caryr", "A1,A2,A3,A4,A5", 5);

        // Set the Battleship
        System.out.println(playerName + ", please give Battleship location?");
        Ship battleship = new Ship("Battl", "D6,E6,F6,G6", 4);

        // Set the Destroyer
        System.out.println(playerName + ", please give Destroyer location?");
        Ship destroyer = new Ship("Dstry", "I2,I3,I4", 3);

        // Set the Submarine
        System.out.println(playerName + ", please give Submarine location?");
        Ship submarine = new Ship(" Sub ", "B8,B9,B10", 2);

        // Set the Patrol Boat
        System.out.println(playerName + ", please give Patrol Boat location?");
        Ship patrol = new Ship("Patrl", "I10", 1);

        Ship[] ships = { carier, battleship, destroyer, submarine, patrol };
        playerShips = ships;
        playerBoard.placeShips(ships);
        playerBoard.print(true);

        System.out.println();
        System.out.println(playerName + ", do you accept these ship placements? (y/n)");
        String accpectShipPlacements = sc.nextLine();

        if (accpectShipPlacements.equalsIgnoreCase("y"))
        {
            System.out.println("Great, let's get started.");
        }
        else
        {
            System.out.println("Too bad, moving on...");
        }
    }

    public boolean allShipsSunk()
    {
        for (Ship ship : playerShips) {
            if (!ship.isSunk())
            {
                return false;
            }
        }
        return true;
    }

    public String callShot()
    {
        // Fire a shot on the board
        System.out.println();
        System.out.println(playerName + ", call your shot...");
        shotFired = sc.next();
        shotFired = shotFired.toUpperCase();
        return shotFired;        
    }

    public void takeShot(String shotLocation)
    {
        System.out.println();
        playerBoard.fireShot(shotLocation);
    }

    public void printBoard()
    {
        playerBoard.print(false);
    }
}